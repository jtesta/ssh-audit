ssh-audit (3.1.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #1059739)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 31 Dec 2023 16:59:15 +0800

ssh-audit (3.0.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #1051610)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 10 Sep 2023 23:00:50 +0800

ssh-audit (2.9.0-1) unstable; urgency=medium

  [ Joe Testa ]
  * Switched to upstream manpage.

  [ ChangZhuo Chen (陳昌倬) ]
  * New upstream release. (Closes: #1035528)
  * Bump Standards-Version to 4.6.2.
  * Add d/upstream/metadata.
  * Update d/watch.
  * Update d/copyright.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 28 Jun 2023 02:59:37 +0800

ssh-audit (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0.
  * Update upstream signing-key.asc.
  * Bump Standards-Version to 4.6.0.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 29 Aug 2021 21:20:48 +0800

ssh-audit (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0.
    * Change upstream to jtesta's fork. (Closes: #940833)
    * Update copyright.
  * Bump Standards-Version to 4.5.0.
    * Add signature check for upstream tarball.
    * Add Rules-Requires-Root to no.
  * Bump compat to 13.
  * Update Vcs-* fields to salsa (Closes: #940834)
  * Check nocheck in DEB_BUILD_OPTIONS.
  * Remove deprecated test cases as upstream suggested.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 03 May 2020 20:09:18 +0800

ssh-audit (1.7.0-2) unstable; urgency=medium

  * Fix Vcs-Git Field.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 29 Jan 2017 16:54:13 +0800

ssh-audit (1.7.0-1) unstable; urgency=medium

  * Initial release (Closes: #841035)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 22 Jan 2017 23:51:46 +0800
